/**
 * Created by Алина on 15.01.2022.
 */

let form = document.querySelector(".password-form");
form.addEventListener("click", (event) => {
    let target = event.target;
    if (target.tagName === "I") {
        target.classList.toggle("fa-eye-slash");
        target.classList.toggle("fa-eye");
        let input = target.previousElementSibling;
        if (target.classList.contains("fa-eye-slash")) {
            input.setAttribute("type", "password")
        } else {
            input.setAttribute("type", "text")
        }
    }
})

let submitBtn = document.querySelector(".btn");
submitBtn.addEventListener("click", (event) => {
    let firstPassword = document.getElementById("enter");
    let secondPassword = document.getElementById("check");
    let error = document.querySelector(".error");
    if (!firstPassword.value || !secondPassword.value ||
        firstPassword.value !== secondPassword.value) {
        event.preventDefault();
        if (!error) {
            let errorMessage = document.createElement("span");
            secondPassword.after(errorMessage);
            errorMessage.classList.add("error")
            errorMessage.innerText = "Нужно ввести одинаковые значения";
            errorMessage.style.color = "red";
        }
    } else {
        if (error) {
            error.remove();
        }
        alert("You are welcome!");
        form.reset()
    }
})
